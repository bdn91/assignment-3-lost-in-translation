import {BASE_URL, checkUsername} from '../login/LoginAPI'

export function createTranslations(user, translation) {
    return fetch(`${BASE_URL}/users/${user.id}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({translation: translation})
    })
        .then(r => r.json())
}
