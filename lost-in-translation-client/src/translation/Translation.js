import AppHeader from "../common/AppHeader"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowCircleRight, faKeyboard } from '@fortawesome/free-solid-svg-icons'
import { useContext, useEffect, useState, forceUpdate } from 'react'
import { useHistory } from 'react-router-dom'
import { LoginContext } from '../context/LoginContext'
import { createTranslations } from './TranslateAPI'
import { checkUsername } from '../login/LoginAPI'
import { dictonary } from './Dictionary'


const Translation = () => {
    let history = useHistory()
    const [translationText, setTranslationText] = useState('')
    // const [ASL] = useState('')
    const [theArray, setTheArray] = useState([]);

    const { username, setUsername } = useContext(LoginContext)
    const onTranslateClick =  async() => {
        try {
            const foundUser = await checkUsername(username)
            const translations = await foundUser.translation
            translations.push(translationText);
            createTranslations(foundUser, translations);

        }
        catch (e) {
            console.log(e)
        }

        const newArray = [];
        [...translationText].forEach(char => {
            if (Object.keys(dictonary).includes(char)) {
                newArray.push(<img alt={dictonary[char]} key={newArray.length} src={dictonary[char]} />)
            }
        })
        setTheArray(newArray);


    }

    useEffect(() => {
        if (!sessionStorage.getItem('username')) {
            history.push('/')
        }
        setUsername(sessionStorage.getItem('username'))
    });

    return (
        <>
            <AppHeader headerLogo="none" />
            <div className="translation-body">
                <div className="translation-input-container">
                    <div className="translation-input-wrapper">
                        <div className="translation-keyboard">
                            <FontAwesomeIcon icon={faKeyboard} />
                        </div>
                        <div className="translation-input-text-wrapper">
                            <input className="translation-input-text" type="text"
                                placeholder="Write"
                                onChange={e => setTranslationText((e.target.value).toLowerCase())} />
                        </div>
                        <div>
                            <FontAwesomeIcon className="translation-input-arrow" icon={faArrowCircleRight} onClick={onTranslateClick} />
                        </div>
                    </div>
                </div>
            </div>
            <div className="translation-field-container">
                <div className="translation-field">
                    {theArray}
                </div>
            </div>

        </>
    )
}

export default Translation