import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { useHistory } from 'react-router-dom'
import { useContext } from 'react'
import {LoginContext} from '../context/LoginContext'

const AppHeader = (props) => {
    const {username, setUsername} = useContext(LoginContext)

    let history = useHistory();
    
    const logout = () => {
        sessionStorage.clear();
        setUsername('')
        history.push("/")
        
    }



    return (
        <header>
            <div className="AppHeader">
                <div className={props.headerLogo}><img src={props.logoSrc} className={props.imgLogo} alt="Logo" /></div>
                <div className="header-title">Lost in translation</div>
                <div className="header-user">
                    <span className="header-username">{username}</span>
                    <div className="header-profile"><FontAwesomeIcon icon={faUser} /></div>
                    <div className="header-logout"><FontAwesomeIcon icon={faSignOutAlt} 
                        onClick={logout} /></div>
                </div>
            </div>
        </header>
    )
}

export default AppHeader