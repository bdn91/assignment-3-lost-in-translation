import Logo from '../img/Logo.png'

const LoginBody = () => {
    return (
        <div className="login-container">
            <div className="login-body-container">
                <div className="login-logo">
                    <img src={Logo} alt="logo" />
                </div>
                <div className="login-text">
                    <p>Lost in Translation</p>
                    <p>Get Started</p>
                </div>
            </div>
        </div>
    )
}

export default LoginBody;