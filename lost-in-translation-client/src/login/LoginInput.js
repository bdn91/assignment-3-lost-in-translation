import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowCircleRight, faKeyboard } from '@fortawesome/free-solid-svg-icons'
import { checkUsername, createUsername } from './LoginAPI';
import { Link } from 'react-router-dom'
import { useContext } from 'react'
import { LoginContext } from '../context/LoginContext'
import { useHistory } from 'react-router-dom'


const LoginInput = () => {
    const { username, setUsername } = useContext(LoginContext)
    let history = useHistory()
    const onLoginClick = async () => {
        try {
            const foundUser = await checkUsername(username)
            if (foundUser) {
                setUsername(username)
                return;
            }
            else {
                console.log(username)
                const createdUser = await createUsername(username)
            }
        }
        catch (e) {
            console.log(e)
        } finally {
            sessionStorage.setItem('username', username);
            history.push('/translate')
        }
    }
    return (
        <div className="login-input-container">
            <div className="login-input-wrapper">
                <div>
                    <FontAwesomeIcon icon={faKeyboard} />
                </div>
                <div>
                    <input className="login-input-text" type="text"
                        placeholder="What's your name?"
                        onChange={e => setUsername(e.target.value)}
                        required
                    />
                </div>
                <div>
                    <Link to="/translate">
                        <FontAwesomeIcon icon={faArrowCircleRight} onClick={onLoginClick} />
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default LoginInput;