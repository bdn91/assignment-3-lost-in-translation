import AppHeader from '../common/AppHeader'
import LoginBody from './LoginBody'
import LoginInput from './LoginInput'
import Logo from '../img/Logo.png'
import { useHistory } from "react-router-dom";

const LoginIndex = () => {
    let history = useHistory();
    if(sessionStorage.getItem('username')){
        history.push('/translate')
    }
    

    return (
        <>
            <AppHeader headerLogo="header-logo" logoSrc={Logo} imgLogo="img-logo"/>
            <LoginBody />
            <LoginInput />
        </>
    )
}
export default LoginIndex