import LoginIndex from "./login/LoginIndex";
import Translation from "./translation/Translation";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { LoginContext } from './context/LoginContext'
import { useState } from 'react'



function App() {

  const [username, setUsername] = useState('');

  return (
    <Router>
      <div className="App">
        <LoginContext.Provider value={{ username, setUsername }}>
          <Switch>
            <Route path="/" exact component={LoginIndex} />
            <Route path="/translate" component={Translation} />
          </Switch>
        </LoginContext.Provider>
      </div>
    </Router>
  );

}

export default App;
